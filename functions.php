<?php

    function isActive($pagename){
        if(basename($_SERVER['PHP_SELF'])==$pagename){
            echo "active";
        }
    }

    function isUserLogged($dbh){
        $user = getUser();
        if(empty($user)) return false;
        $res = $dbh->getUtente($user["username"])[0];
        return $user["password"] == $res["password"];
    }

    function setLogin($user){
        $_SESSION["user"] = $user;
    }

    function getUser(){
        return isset($_SESSION["user"]) ? $_SESSION["user"] : array();
    }
?>