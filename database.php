<?php
class DatabaseHelper{
    private $db;

    public function __construct($servername, $username, $password, $dbname, $port){
        $this->db = new mysqli($servername, $username, $password, $dbname, $port);
        if ($this->db->connect_error) {
            die("Connection failed: " . $db->connect_error);
        }        
    }

    public function addProduct($nome, $venditore, $prezzo, $formato, $alcohol, $imgPath){
        $stmt = $this->db->prepare("SELECT nome FROM prodotto WHERE venditore = ? AND nome = ?");
        $stmt->bind_param('ss', $venditore, $nome);
        $stmt->execute();
        if (mysqli_num_rows($stmt->get_result()) != 0) {   
            return 1; //error, product name already taken
        } else {
            $stmt->reset();
            $stmt = $this->db->prepare("INSERT INTO prodotto VALUES (?, ?, ?, ?, ?, ?, 0)");
            $stmt->bind_param('ssdsss', $nome, $venditore, $prezzo, $formato, $alcohol, $imgPath);
            $stmt->execute();
            return 0;
        }
    }

    public function removeProduct($nome, $venditore){
        $stmt = $this->db->prepare("SELECT nome FROM prodotto WHERE venditore = ? AND nome = ?");
        $stmt->bind_param('ss', $venditore, $nome);
        $stmt->execute();
        if (mysqli_num_rows($stmt->get_result()) == 0) {   
            return 1; //error, no product in db with that name
        } else {
            $stmt->reset();
            $stmt = $this->db->prepare("DELETE FROM prodotto WHERE venditore = ? AND nome = ?");
            $stmt->bind_param('ss', $venditore, $nome);
            $stmt->execute();
            return 0;
        }
    }

    public function getVendorProducts($username) {
        $stmt = $this->db->prepare("SELECT nome, prezzo, formato, alcohol, img_path, disponibili FROM prodotto WHERE venditore = ?");
        $stmt->bind_param('s', $username);
        $stmt->execute();
        return $stmt->get_result()->fetch_all(MYSQLI_ASSOC);
    }

    public function getAllProducts() {
        $stmt = $this->db->prepare("SELECT nome, venditore, prezzo, formato, alcohol, img_path, disponibili FROM prodotto");
        $stmt->execute();
        return $stmt->get_result()->fetch_all(MYSQLI_ASSOC);
    }

    public function getBasket($username) {
        $stmt = $this->db->prepare("SELECT p.nome, p.venditore, p.prezzo, p.formato, p.alcohol, p.img_path, p.disponibili, c.quantità FROM prodotto p INNER JOIN carrello c ON c.venditore = p.venditore WHERE c.utente = ? AND p.nome = c.prodotto");
        $stmt->bind_param('s', $username);
        $stmt->execute();
        return $stmt->get_result()->fetch_all(MYSQLI_ASSOC);
    }

    public function addToBasket($prodotto, $venditore, $utente, $qt){
        $stmt = $this->db->prepare("SELECT quantità FROM carrello WHERE venditore = ? AND prodotto = ? AND utente = ?");
        $stmt->bind_param('sss', $venditore, $prodotto, $utente);
        $stmt->execute();
        $prevqt = $stmt->get_result();
        if (mysqli_num_rows($prevqt) != 0) { 
            $stmt->reset();
            $stmt = $this->db->prepare("UPDATE carrello SET quantità = ? WHERE venditore = ? AND prodotto = ? AND utente = ?");
            $stmt->bind_param('dsss', $qt, $venditore, $prodotto, $utente);
            $stmt->execute();
            return 1; //product is already in basket
        } else {
            $stmt->reset();
            $stmt = $this->db->prepare("INSERT INTO carrello VALUES (?, ?, ?, ?)");
            $stmt->bind_param('sssi', $utente, $prodotto, $venditore, $qt);
            $stmt->execute();
            return 0;
        }
    }

    public function rmvFromBasket($prodotto, $venditore, $utente){
        $stmt = $this->db->prepare("DELETE FROM carrello WHERE venditore = ? AND prodotto = ? AND utente = ?");
        $stmt->bind_param('sss', $venditore, $prodotto, $utente);
        $stmt->execute();
    }

    public function getQuantity($nome, $venditore) {
        $stmt = $this->db->prepare("SELECT disponibili FROM prodotto WHERE nome = ? AND venditore = ?");
        $stmt->bind_param('ss', $nome, $venditore);
        $stmt->execute();
        return $stmt->get_result()->fetch_all(MYSQLI_ASSOC);
    }

    public function setQuantity($disponibili, $nome, $venditore) {
        $stmt = $this->db->prepare("UPDATE prodotto SET disponibili = ? WHERE nome = ? AND venditore = ?");
        $stmt->bind_param('iss', $disponibili, $nome, $venditore);
        $stmt->execute();
    }

    public function getUtente($username) {
        $stmt = $this->db->prepare("SELECT nome, cognome, password, username, venditore FROM utente WHERE username = ?");
        $stmt->bind_param('s', $username);
        $stmt->execute();
        return $stmt->get_result()->fetch_all(MYSQLI_ASSOC);
    }

    public function checkLogin($username, $password){
        $query = "SELECT password FROM utente WHERE username = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('s',$username);
        $stmt->execute();
        $result = $stmt->get_result()->fetch_all(MYSQLI_ASSOC);
        if(empty($result)) return false;
        return password_verify($password, $result[0]["password"]);
    }  

    public function registerUser($nome, $cognome, $username, $password, $venditore){
        $stmt = $this->db->prepare("SELECT username FROM utente WHERE username = ?");
        $stmt->bind_param('s',$username);
        $stmt->execute();
        if (mysqli_num_rows($stmt->get_result()) != 0) {   
            return 1; //error, name already taken
        } else {
            $stmt->reset();
            $stmt = $this->db->prepare("INSERT INTO utente VALUES (?, ?, ?, ?, ?)");
            $stmt->bind_param('sssss', $nome, $cognome, $username, $password, $venditore);
            $stmt->execute();
            return 0;
        }
    }
    
    public function addNotification($user, $type, $text, $title){
        $stmt = $this->db->prepare("INSERT INTO notifica (utente, tipo, testo, titolo)  VALUES (?, ?, ?, ?)");
        $stmt->bind_param('ssss', $user, $type, $text, $title);
        $stmt->execute();
    }

    public function removeNotification($user, $id){
        $stmt = $this->db->prepare("DELETE FROM notifica WHERE utente = ? and id = ?");
        $stmt->bind_param('si', $user, $id);
        $stmt->execute();
    }

    public function wipeNotifications($user){
        $stmt = $this->db->prepare("DELETE FROM notifica WHERE utente = ?");
        $stmt->bind_param('s', $user);
        $stmt->execute();
    }

    public function getNotifications($user){
        $stmt = $this->db->prepare("SELECT id, tipo, testo, titolo FROM notifica WHERE utente = ?");
        $stmt->bind_param('s', $user);
        $stmt->execute();
        return $stmt->get_result()->fetch_all(MYSQLI_ASSOC);
    }

}
?>