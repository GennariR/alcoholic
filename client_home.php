<?php
require_once './initializer.php';

$templateParams["titolo"] = "Cliente - Alcoholic";
$templateParams["nome"] = "client_home.php";
$templateParams["js"] = array("js/filterProducts.js", "js/blinkText.js");
$templateParams["notifications"] = $dbh->getNotifications(getUser()["username"]);

$templateParam["product"] = $dbh->getAllProducts();

if(isset($_POST["nome"], $_POST["venditore"])) {
    if(!$dbh->addToBasket($_POST["nome"], $_POST["venditore"], getUser()["username"], $_POST["qt"])){
        $templateParams["addbasketoutcome"] = "<p class=\"text-success m-3\">Prodotto aggiunto al carrello</p>";
    } else {
        $templateParams["addbasketoutcome"] = "<p class=\"text-success m-3\">Quantità nel carrello aggiornata!</p>";
    }
    $templateParams["addbasketname"] = $_POST["nome"];
    $templateParams["addbasketvendor"] = $_POST["venditore"];
}

require("./template/base.php");
?>