<?php
require_once './initializer.php';

$templateParams["titolo"] = "Venditore - Alcoholic";
$templateParams["nome"] = "vendor_manage_products.php";
$templateParams["js"] = array("js/switchTab.js", "js/imagePreview.js", "js/blinkText.js");
$templateParams["notifications"] = $dbh->getNotifications(getUser()["username"]);
$templateParams["showadd"] = true;

if(isset($_POST["addProductName"], $_POST["addProductPrice"], $_POST["addProductFormat"], $_POST["addProductAlcohol"]) && (isset($_FILES["addProductImage"]) && $_FILES["addProductImage"]["size"]>0)){

    $imgPath = UPLOAD_DIR.getUser()["username"]."_".$_FILES['addProductImage']["name"];
    move_uploaded_file($_FILES['addProductImage']["tmp_name"], $imgPath);

    switch($dbh->addProduct($_POST["addProductName"], getUser()["username"], $_POST["addProductPrice"], $_POST["addProductFormat"], $_POST["addProductAlcohol"], $imgPath)){
        case 0:
            $templateParams["regoutcome"] = "<p class=\"text-success m-3\">Prodotto registrato con successo!</p>";
            break;
        case 1:
            $templateParams["regoutcome"] = "<p class=\"text-danger m-3\">Errore: nome prodotto già registrato. Scegline un altro!</p>";
            break;
        default:
            $templateParams["regoutcome"] = "<p class=\"text-danger m-3\">C'è stato un errore nella registrazione. Riprova più tardi!</p>";
            break;
    }
} else if(isset($_POST["remselect"]) && ($_POST["remselect"] != "seleziona un prodotto da rimuovere")){
    $templateParams["showrem"] = true;
    unset($templateParams["showadd"]);

    if($dbh->removeProduct($_POST["remselect"], getUser()["username"])){
        $templateParams["remoutcome"] = "<p class=\"text-danger m-3\">C'è stato un errore nella rimozione. Aggiorna la pagina e riprova!</p>";
    } else {
        $templateParams["remoutcome"] = "<p class=\"text-success m-3\">Prodotto rimosso con successo!</p>";
    }
}

$pnames = $dbh->getVendorProducts(getUser()["username"]);
if(!empty($pnames)){
    $templateParams["prodNames"] = array_column($pnames, "nome");
}

require("./template/base.php");
?>