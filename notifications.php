<?php
require_once './initializer.php';

$templateParams["titolo"] = "Notifiche - Alcoholic";
$templateParams["nome"] = "notifications.php";
$templateParams["js"] = array();

$templateParams["wipeNotifications"] = true;

if(isset($_POST["rmid"])){
    $dbh->removeNotification(getUser()["username"], $_POST["rmid"]);
} else if(isset($_POST["wipe"])){
    $dbh->wipeNotifications(getUser()["username"]);
}

$templateParams["notifications"] = $dbh->getNotifications(getUser()["username"]);

require("./template/base.php");
?>