<!DOCTYPE html> 
<html lang="it">
    <head>
        <meta charset="utf-8">
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
        <title><?php echo $templateParams["titolo"]; ?></title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?php if(isset($templateParams["js"])):
                foreach($templateParams["js"] as $script):
        ?>
                <script src="<?php echo $script; ?>"></script>
        <?php
                endforeach;
            endif;
        ?>
    </head>
    <body class="d-flex flex-column min-vh-100" style="padding-top:61.38px;">
        <nav class="navbar navbar-expand-lg navbar-dark bg-primary fixed-top p-0 ">
            <div class="nav-item mr-auto ml-4">
                <a href="#" class="navbar-brand pb-0 display-1">Alcoholic</a>
                <p class="small text-muted pt-0">Your dealer in bad habits</p>
            </div>
            <?php if(isUserLogged($dbh)) echo("<button class=\"navbar-toggler mr-5\" type=\"button\" data-toggle=\"collapse\" data-target=\"#main\" aria-controls=\"main\" aria-expanded=\"false\" aria-label=\"Toggle navigation\">
            <span class=\"navbar-toggler-icon\"></span>
            </button>");?>
            <div class="collapse navbar-collapse mx-auto" id="main">
                <?php if(isUserLogged($dbh)) require((getUser()["venditore"]) ? "./template/vendor_navbar.php" : "./template/client_navbar.php"); ?>   
            </div>
        </nav>
        <main>
            <?php ((isUserLogged($dbh) && isset($templateParams["nome"])) || (basename($_SERVER['PHP_SELF']) == "login.php")) ? require($templateParams["nome"]) : header('Location: login.php');?>
        </main>
        <footer class="container-fluid mt-auto ml-0 mr-0">
            <?php if(isset($templateParams["confirmOrder"]))require("confirm_order.php");?>
            <?php if(isset($templateParams["wipeNotifications"]))require("wipe_notifications.php");?>
            <section class="row text-center">
                <a href="#top" class="text-muted bg-primary pt-3 pb-2 col-12">Torna in cima</a>
            </section>
        </footer>
    </body>
</html>