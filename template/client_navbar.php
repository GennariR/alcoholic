<ul class="navbar-nav mx-auto">
    <li class="nav-item <?php isActive("client_home.php");?>">
        <a class="nav-link ml-4 ml-lg-0" href="./client_home.php">Home</a>
    </li>
    <li class="nav-item <?php isActive("basket.php");?>">
        <a class="nav-link ml-4 ml-lg-0" href="./basket.php">Carrello</a>
    </li>
    <li class="nav-item <?php isActive("notifications.php");?>">
        <a class="nav-link ml-4 ml-lg-0 <?php if(!empty($templateParams["notifications"])) echo("blink");?>" href="./notifications.php">Notifiche</a>
    </li>
</ul>
<ul class="navbar-nav ml-4 mt-3 mt-lg-0 ml-lg-5">
    <li class="nav-item ">
        <a class="nav-link" href="./logout.php">Logout</a>
    </li>
</ul> 