<section class ="container-fluid p-0">
  <ul class="list-group list-group-flush">
    <?php foreach($templateParams["notifications"] as $n) {
        echo("<li class=\"list-group-item w-100 p-0\">
                <div class=\"container-fluid card text-white ".$n["tipo"]."\">
                    <div class=\"card-body row\">
                        <div class=\"col-10\">
                            <h4 class=\"card-title\">".$n["titolo"]."</h4>
                            <p class=\"card-text\">".$n["testo"]."</p>
                        </div>
                        <div class=\"col-2\">
                            <form class=\"\" action=\"\" method=\"post\">
                                <div class=\"text-right\">
                                    <input type=\"hidden\" class=\"form-control\" value=\"".$n["id"]."\" name=\"rmid\">
                                    <button type=\"submit\" class=\"btn btn-danger btn-sm mt-3\">Elimina</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </li>");
    }
    if(empty($n)){echo("<li class=\"list-group-item w-100 p-0\">
            <div class=\"container-fluid card text-white bg-info\">
                <div class=\"card-body row\">
                    <div class=\"col-12 text-center\">
                        <h4 class=\"card-title\"></h4>
                        <p class=\"card-text\">Nessuna notifica!</p>
                    </div>
                </div>
            </div>
        </li>");}
    ?>
  </ul>
</section>