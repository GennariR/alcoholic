<section class="d-flex flex-column align-items-center">
  <ul class="nav nav-tabs pt-3 w-100 nav-justified bg-dark">
    <li class="nav-item">
      <a class="nav-link js-tab <?php if(isset($templateParams["showadd"])){echo("active");}?>" data-toggle="tab" id="addTab" href="#add"><h6 class="text-white" id="addTabText">Aggiungi prodotto</h6></a>
    </li>
    <li class="nav-item">
      <a class="nav-link js-tab <?php if(isset($templateParams["showrem"])){echo("active");}?>" data-toggle="tab" id="remTab" href="#rem"><h6 class="text-white" id="remTabText">Rimuovi prodotto</h6></a>
    </li>
  </ul>
  <div class="tab-content row w-100">
    <div class="tab-pane fade col-lg-6 col-sm-12 <?php if(isset($templateParams["showadd"])){echo("active show");}?>" id="add">
        <form action="" method="post" enctype="multipart/form-data" autocomplete="off">
            <fieldset class="form-group p-3">
              <label for="addProductName">Nome</label>
              <input type="text" class="form-control" name="addProductName" placeholder="inserisci nome prodotto" required>
              <label for="addProductPrice">Prezzo</label>
              <input type="number" class="form-control" name="addProductPrice" step="0.01" min="0.01" max="10000" placeholder="inserisci prezzo" required>
              <label for="addProductFormat">Formato</label>
              <input type="text" class="form-control" name="addProductFormat" placeholder="inserisci formato (capacità bottiglia in litri)" required>
              <label for="addProductAlcohol">Tasso alcolico</label>
              <input type="number" class="form-control mb-5" min="0" max="100" name="addProductAlcohol" placeholder="inserisci tasso alcolico (%)" required>
              <label for="addProductImage">Immagine prodotto</label>
              <input type="file" class="form-control-file center" name="addProductImage" id="productImage" accept=".jpg, .png, .jpeg" aria-describedby="fileHelp" required>
              <small id="fileHelp" class="form-text text-muted">Inserisci un'immagine per il prodotto (jpg, png, jpeg)</small>
              <div class="text-center">
                <div id="imgPreviewArea"></div>
                <button type="submit" class="btn btn-primary m-3">Aggiungi</button>
                <?php if(isset($templateParams["regoutcome"])){echo($templateParams["regoutcome"]);}?>
              </div>
            </fieldset>
        </form>
    </div>
    <div class="tab-pane fade col-lg-6 col-sm-12 ml-auto <?php if(isset($templateParams["showrem"])){echo("active show");}?>" id="rem">
        <form action="" method="post">
          <fieldset class="form-group p-3">
            <select class="form-control" name="remselect" required>
            <optgroup>
              <option hidden disabled selected value>seleziona un prodotto da rimuovere</option>
              <?php if(isset($templateParams["prodNames"])) foreach ($templateParams["prodNames"] as $key=>$item){echo("<option>".$item."</option>");}?>
            </optgroup>
            </select>
            <div class="text-center">
              <button type="submit" class="btn btn-primary m-3">Rimuovi</button>
              <?php if(isset($templateParams["remoutcome"])){echo($templateParams["remoutcome"]);}?>
            </div>
          </fieldset>
        </form>
    </div>
  </div>
</section>