<ul class="navbar-nav mx-auto">
    <li class="nav-item ml-4 ml-lg-0 <?php isActive("vendor_your_products.php");?>">
        <a class="nav-link" href="vendor_your_products.php">I tuoi prodotti</a>
    </li>
    <li class="nav-item ml-4 ml-lg-0 <?php isActive("vendor_manage_products.php");?>">
        <a class="nav-link" href="vendor_manage_products.php">Gestisci prodotti</a>
    </li>
    <li class="nav-item ml-4 ml-lg-0 <?php isActive("notifications.php");?>">
        <a class="nav-link <?php if(!empty($templateParams["notifications"])) echo("blink");?>" href="notifications.php">Notifiche</a>
    </li>
</ul>
<ul class="navbar-nav ml-4 mt-3 mt-lg-0 ml-lg-5">
    <li class="nav-item">
        <a class="nav-link pr-4" href="./logout.php">Logout</a>
    </li>
</ul> 