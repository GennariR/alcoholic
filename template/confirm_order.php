<section class="bg-dark pt-3 pb-3 row <?php if(empty($templateParams["product"])){echo("d-none");}?>">
  <form class="col-12 pl-0 pr-0 <?php if(empty($templateParams["product"])){echo("d-none");}?>" action="" method="post">
    <input type="hidden" value="true" name="ordina" title="ordina">
    <div class="d-flex justify-content-center">
    <button type="submit" class="btn btn-success btn-lg rounded">Conferma l'ordine<br>Totale: <?php $tot=0; foreach($templateParams["product"] as $product){$tot+=($product["prezzo"]*$product["quantità"]);} echo($tot);?>€</button>
    </div>
  </form>
</section>