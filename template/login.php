<section class="d-flex flex-column align-items-center">
  <ul class="nav nav-tabs pt-3 w-100 nav-justified bg-dark">
    <li class="nav-item">
      <a class="nav-link js-tab <?php if(isset($templateParams["showlogin"])){echo("active");}?>" data-toggle="tab" id="loginTab" href="#login"><h6 class="text-white" id="loginTabText">Accedi</h6></a>
    </li>
    <li class="nav-item">
      <a class="nav-link js-tab <?php if(isset($templateParams["showregister"])){echo("active");}?>" data-toggle="tab" id="registerTab" href="#register"><h6 class="text-white" id="registerTabText">Registrati</h6></a>
    </li>
  </ul>
  <div class="tab-content row w-100">
    <div class="tab-pane fade col-lg-6 col-sm-12 <?php if(isset($templateParams["showlogin"])){echo("active show");}?>" id="login">
        <form action="" method="post" autocomplete="off">
            <fieldset class="form-group p-3">
                <label for="username">Username</label>
                <input type="text" class="form-control" id="username" name="username" placeholder="inserisci username" required>
                <label for="password">Password</label>
                <input type="password" class="form-control" id="password" name="password" placeholder="inserisci password" required>
                <div class="text-center mt-4">
                  <button type="submit" class="btn btn-primary m-3">Login</button>
                  <?php if(isset($templateParams["errorelogin"])){echo("<p class=\"text-danger col m-3\">Credenziali errate, riprova o registrati se non hai ancora un account</p>");}?>
                </div>
            </fieldset>
        </form>
    </div>
    <div class="tab-pane fade col-lg-6 col-sm-12 ml-auto <?php if(isset($templateParams["showregister"])){echo("active show");}?>" id="register">
        <form action="" method="post" autocomplete="off">
            <fieldset class="form-group p-3">
                <label for="nameReg">Nome</label>
                <input type="text" class="form-control" id="nameReg" name="nameReg" placeholder="inserisci nome" required>
                <label for="surnameReg">Cognome</label>
                <input type="text" class="form-control" id="surnameReg" name="surnameReg" placeholder="inserisci cognome" required>
                <label for="usernameReg">Username</label>
                <input type="text" class="form-control" id="usernameReg" name="usernameReg" placeholder="inserisci username" required>
                <label for="passwordReg">Password</label>
                <input type="password" class="form-control" id="passwordReg" name="passwordReg" placeholder="inserisci password" required>
                <div class="form-check text-center">
                  <label for="vendor" class="form-check-label mt-5"><input id="vendor" class="form-check-input" type="checkbox" id="vendor" name="vendor">Registrarsi come venditore?</label>
                </div>
                <div class="text-center">
                  <button type="submit" class="btn btn-primary m-3">Registrati</button>
                  <?php if(isset($templateParams["regoutcome"])){echo($templateParams["regoutcome"]);}?>
                </div>
            </fieldset>
          </form>
    </div>
  </div>
</section>