<section class="bg-dark pt-3 pb-3 row <?php if(empty($templateParams["notifications"])){echo("d-none");}?>">
  <form class="col-12 pl-0 pr-0" action="" method="post">
    <input title="cancella tutte notifiche" type="hidden" value="0" name="wipe">
    <div class="d-flex justify-content-center">
      <button type="submit" class="btn btn-danger btn-lg rounded">Cancella tutto</button>
    </div>
  </form>
</section>