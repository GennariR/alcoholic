<section class ="container-fluid p-0">
  <?php require("search_bar.php");?>
  <ul class="d-flex flex-wrap p-0 mb-0">
    <?php
      foreach($templateParams["product"] as $product){
        echo("<li class=\"prod list-group-item col-lg-4 p-0 border-primary\" id=\"".$product["nome"]."\">
        <h4 class=\"text-center bg-primary p-3 text-white\">".$product["nome"]."</h4>
        <div class=\"container row pt-5 pb-5 pl-0 pr-0 m-0\">
            <img src=\"".$product["img_path"]."\" class=\"img-fluid col-4 p-0\" alt=\"".$product["nome"]."\">
            <article class=\"col-4 mt-4\">
              <p class=\"font-weight-bold\">Venditore: <span class=\"font-weight-normal\">".$product["venditore"]."</span></p>
              <p class=\"font-weight-bold\">Prezzo: <span class=\"font-weight-normal\">".$product["prezzo"]."€</span></p>
              <p class=\"font-weight-bold\">Formato: <span class=\"font-weight-normal\">".$product["formato"]."</span></p>
              <p class=\"font-weight-bold\">Alcohol: <span class=\"font-weight-normal\">".$product["alcohol"]."%</span></p>
              <p class=\"font-weight-bold\">Disponibili: <span class=\"font-weight-normal\">".$product["disponibili"]."</span></p>
            </article>
            <form class=\"col-4 mt-5 p-0\" action=\"\" method=\"post\">
              <p>Quantità ordinata: ".$product["quantità"]."</p>
              <p class=\"mb-4\">Subtotale: ".$product["quantità"]*$product["prezzo"]."€</p>
              <input title=\"nome prodotto\" type=\"hidden\" class=\"form-control\" value=\"".$product["nome"]."\" name=\"nome\">
              <input title=\"venditore\" type=\"hidden\" class=\"form-control\" value=\"".$product["venditore"]."\"name=\"venditore\">
              <button type=\"submit\" class=\"btn btn-danger btn-sm mb-3\">Rimuovi dal carrello</button>
            </form>
          </div>
        </li>");
      };
      if(isset($templateParams["ordine"])){echo("<li class=\"list-group-item w-100 p-0\">
          <div class=\"container-fluid card text-white bg-success\">
            <div class=\"card-body row\">
              <div class=\"col-12\">
                  <h4 class=\"card-title\"></h4>
                  <p class=\"card-text\">Ordine confermato!</p>
              </div>
            </div>
          </div>
      </li>");} else if(empty($templateParams["product"])){echo("<li class=\"list-group-item w-100 p-0\">
        <div class=\"container-fluid card text-white bg-info\">
          <div class=\"card-body row\">
            <div class=\"col-12\">
              <h4 class=\"card-title\"></h4>
              <p class=\"card-text\">Nessun prodotto nel carrello!</p>
            </div>
        </div>
      </div>
    </li>");}
    
    ?>
  </ul>
</section>
