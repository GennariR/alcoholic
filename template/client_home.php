<section class ="container-fluid p-0">
  <?php require("search_bar.php");?>
  <ul class="d-flex flex-wrap p-0 mb-0">
    <?php
      foreach($templateParam["product"] as $product){
        echo("<li class=\"prod list-group-item col-lg-4 p-0 border-primary\" id=\"".$product["nome"]."\">
          <h4 class=\"text-center bg-primary p-3 mb-0 text-white\">".$product["nome"]."</h4>
          <div class =\"container row align-items-center p-0 m-0\" style=\"height:300px;\">
                <img src=\"".$product["img_path"]."\" class=\"img-fluid col-4 p-0\" style=\"max-height:290px;\" alt=\"".$product["nome"]."\">
                <article class=\"col-3 p-0\">
                  <p class=\"font-weight-bold\">Venditore: <span class=\"font-weight-normal\">".$product["venditore"]."</span></p>
                  <p class=\"font-weight-bold\">Prezzo: <span class=\"font-weight-normal\">".$product["prezzo"]."€</span></p>
                  <p class=\"font-weight-bold\">Formato: <span class=\"font-weight-normal\">".$product["formato"]."</span></p>
                  <p class=\"font-weight-bold\">Alcohol: <span class=\"font-weight-normal\">".$product["alcohol"]."%</span></p>
                  <p class=\"font-weight-bold\">Disponibili: <span class=\"font-weight-normal\">".$product["disponibili"]."</span></p>
                </article>");
                echo("<form class=\"col-5 p-0\" action=\"\" method=\"post\">");
                  if($product["disponibili"] > 0) {
                    echo("<fieldset class=\"form-group\">
                      <input title=\"nome prodotto\" type=\"hidden\" class=\"form-control\" value=\"".$product["nome"]."\" name=\"nome\">
                      <input title=\"venditore\" type=\"hidden\" class=\"form-control\" value=\"".$product["venditore"]."\"name=\"venditore\">
                      <input title=\"quantità\" type=\"number\" id=\"".$product["nome"].$product["venditore"]."\" class=\"form-control border-secondary m-3 w-75 qt\" min=1 max=".$product["disponibili"]." name=\"qt\" placeholder=\"inserisci quantità. . .\" required>
                      <button type=\"submit\" id=\"sub".$product["nome"].$product["venditore"]."\" class=\"btn btn-success mt-2 ml-3 w-75\">Aggiungi al carrello</button>
                    </fieldset>");
                    if(isset($templateParams["addbasketname"], $templateParams["addbasketvendor"]) && ($templateParams["addbasketname"] == $product["nome"]) && ($templateParams["addbasketvendor"] == $product["venditore"])){
                      echo($templateParams["addbasketoutcome"]);
                    }
                  } else {
                    echo("<input title=\"quantità disabled\" type=\"number\" class=\"form-control border-secondary m-3 w-75\" disabled placeholder=\"inserisci quantità. . .\">
                          <button type=\"button\" class=\"btn btn-warning disabled mt-3 ml-3 w-75\">Aggiungi al carrello</button>
                          <p class=\"text-danger ml-3\">Prodotto esaurito</p>");
                  }
              echo("</form>
           </div>
        </li>");
      };
    ?>
  </ul>
</section>

