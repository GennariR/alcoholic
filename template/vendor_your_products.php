  <section class ="container-fluid p-0">
  <?php require("search_bar.php");?>
  <ul class="d-flex flex-wrap p-0 m-0">
    <?php 
      foreach($templateParams["product"] as $product){
        echo("<li class=\"prod list-group-item col-lg-4 col-sm-12 border-dark\" id=\"".$product["nome"]."\"> 
          <h3 class=\"text-center p-3 bg-primary text-white\">".$product["nome"]."</h3>
          <div class=\"row\">
              <img src=\"".$product["img_path"]."\" class=\"img-fluid col-6\" style=\"height:300px;\" alt=\"".$product["nome"]."\">
              <article class=\"col-6 pt-3 mt-5\">
                  <p>Prezzo: ".$product["prezzo"]."€</p>
                  <p>Formato: ".$product["formato"]."</p>
                  <p>Alcohol: ".$product["alcohol"]."%</p>
                  <p>Disponibili: ".$product["disponibili"]."</p>
              </article>
          </div>
          <div class=\"list-group list-group-horizontal mt-3\">
            <a href=\"#\" class=\"list-group-item list-group-item-action flex-column align-items-start active mr-3\">
              <h5 class=\"mb-1 text-center\">Aggiungi al magazzino</h5>
              <form class=\"col-12 mt-5\" action=\"\" method=\"post\">
                  <input title=\"nome prodotto\" type=\"hidden\" class=\"form-control\" value=\"".$product["nome"]."\" name=\"addnome\">
                  <input title=\"quantità da aggiungere\" type=\"number\" class=\"form-control \" name=\"addqt\" min=1 max=30000 placeholder=\"quantità...\" required>
                  <button type=\"submit\" class=\"btn btn-success btn-sm col-12\">Aggiungi</button>");
                  if(isset($templateParams["addnome"]) && ($templateParams["addnome"] == $product["nome"])){
                    echo($templateParams["addoutcome"]);
                  }
              echo("</form>
            </a>
            <a href=\"#\" class=\"list-group-item list-group-item-action flex-column align-items-start active\">
            <h5 class=\"mb-1 text-center\">Rimuovi dal magazzino</h5>
            <form class=\"col-12 mt-5\" action=\"\" method=\"post\">
                <input title=\"nome prodotto\" type=\"hidden\" class=\"form-control\" value=\"".$product["nome"]."\" name=\"remnome\">
                <input title=\"quantità da rimuovere\" type=\"number\" class=\"form-control \" name=\"remqt\" min=0 max=".$product["disponibili"]." placeholder=\"quantità...\" required>
                <button type=\"submit\" class=\"btn btn-danger btn-sm col-12\">Rimuovi</button>");
                if(isset($templateParams["remnome"]) && ($templateParams["remnome"] == $product["nome"])){
                  echo($templateParams["remoutcome"]);
                }
            echo("</form>
            </a>
          </div>
        </li>");
      };
      if(empty($templateParams["product"])){echo("<li class=\"list-group-item w-100 p-0\">
        <div class=\"container-fluid card text-white bg-info\">
            <div class=\"card-body row\">
                <div class=\"col-12 text-center\">
                    <h4 class=\"card-title\">Nessun prodotto registrato!</h4>
                    <p class=\"card-text\">Puoi registrare i tuoi prodotti nella sezione \"Gestisci Prodotti\"</p>
                </div>
            </div>
        </div>
    </li>");}
    ?>
  </ul>
</section>
