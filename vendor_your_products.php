<?php
require_once './initializer.php';

$templateParams["titolo"] = "Venditore - Alcoholic";
$templateParams["nome"] = "vendor_your_products.php";
$templateParams["js"] = array("js/filterProducts.js", "js/blinkText.js");
$templateParams["notifications"] = $dbh->getNotifications(getUser()["username"]);


if(isset($_POST["addnome"],$_POST["addqt"])){
    $dbh->setQuantity($dbh->getQuantity($_POST["addnome"], getUser()["username"])[0]["disponibili"] + $_POST["addqt"], $_POST["addnome"], getUser()["username"]);
    $templateParams["addnome"] = $_POST["addnome"];
    $templateParams["addoutcome"] = "<p class=\"text-success m-3\">Prodotti inviati in magazzino</p>";
    $dbh->addNotification(getUser()["username"], "bg-success", "Un carico di ".$_POST["addqt"]." ".$_POST["addnome"]." è stato ricevuto in magazzino! Questi prodotti sono ora in vendita.", "Prodotti arrivati in magazzino");
} else if(isset($_POST["remnome"],$_POST["remqt"])){
    $dbh->setQuantity($dbh->getQuantity($_POST["remnome"], getUser()["username"])[0]["disponibili"] - $_POST["remqt"], $_POST["remnome"], getUser()["username"]);
    $templateParams["remnome"] = $_POST["remnome"];
    $templateParams["remoutcome"] = "<p class=\"text-success m-3\">Prodotti ritirati dal magazzino</p>";
    $dbh->addNotification(getUser()["username"], "bg-success", "Un carico di ".$_POST["remqt"]." ".$_POST["remnome"]." è stato ritirato in magazzino! Questi prodotti non sono più in vendita.", "Prodotti ritirati dal magazzino");
}

$templateParams["product"] = $dbh->getVendorProducts(getUser()["username"]);

require("./template/base.php");
?>