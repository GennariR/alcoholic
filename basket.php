<?php
require_once './initializer.php';

$templateParams["titolo"] = "Carrello - Alcoholic";
$templateParams["nome"] = "basket.php";
$templateParams["js"] = array("js/filterProducts.js", "js/blinkText.js");

$templateParams["notifications"] = $dbh->getNotifications(getUser()["username"]);
$templateParams["confirmOrder"] = true;

if(isset($_POST["nome"], $_POST["venditore"])) {
    $dbh->rmvFromBasket($_POST["nome"], $_POST["venditore"], getUser()["username"]);
}

$templateParams["product"] = $dbh->getBasket(getUser()["username"]);

if(isset($_POST["ordina"])) {
    foreach($templateParams["product"] as $prod) {
        $dbh->setQuantity($dbh->getQuantity($prod["nome"], $prod["venditore"])[0]["disponibili"] - $prod["quantità"], $prod["nome"], $prod["venditore"]);
        if(!$dbh->getQuantity($prod["nome"], $prod["venditore"])[0]["disponibili"]){
            $dbh->addNotification($prod["venditore"], "bg-warning", "Le scorte in magazzino del tuo prodotto ".$prod["nome"]." sono esaurite!.", "Prodotto esaurito");
        }
        $dbh->rmvFromBasket($prod["nome"], $prod["venditore"], getUser()["username"]);
    }
    $names="";
    foreach($templateParams["product"] as $prod){$names = $names.$prod["quantità"]." ".$prod["nome"].", ";}
    $dbh->addNotification(getUser()["username"], "bg-success", "Il tuo ordine comprendente ".$names."è stato confermato e spedito! Il pagamento sarà effettuato al momento della consegna.", "Ordine spedito");
    $templateParams["product"] = $dbh->getBasket(getUser()["username"]);
    $templateParams["ordine"] = true;
}

require("./template/base.php");
?>