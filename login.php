<?php
require_once './initializer.php';

$templateParams["titolo"] = "Login - Alcoholic";
$templateParams["nome"] = "login.php";
$templateParams["js"] = array("js/switchTab.js");

$templateParams["showlogin"] = true;

if(isUserLogged($dbh)){
    if(getUser()["venditore"]){
        header("Location: vendor_your_products.php");
        exit;
    } else {
        header("Location: client_home.php");
        exit;
    }
}

if(isset($_POST["username"], $_POST["password"])){

    if($dbh->checkLogin($_POST["username"], $_POST["password"]) === false){
        $templateParams["errorelogin"] = true;
    } else {
        setLogin($dbh->getUtente($_POST["username"])[0]);
        if(getUser()["venditore"]){
            header("Location: vendor_your_products.php");
            exit;
        } else {
            header("Location: client_home.php");
            exit;
        }
    }
} else if(isset($_POST["usernameReg"], $_POST["passwordReg"], $_POST["nameReg"], $_POST["surnameReg"]) ) {
    $templateParams["showregister"] = true;
    unset($templateParams["showlogin"]);

    switch ($dbh->registerUser($_POST["nameReg"], $_POST["surnameReg"], $_POST["usernameReg"], password_hash($_POST["passwordReg"], PASSWORD_DEFAULT), isset($_POST["vendor"]) ? true : false)){
        case 0:
            $templateParams["regoutcome"] = "<p class=\"text-success m-3\">Registrazione completata con successo!</p>";
            break;
        case 1:
            $templateParams["regoutcome"] = "<p class=\"text-danger m-3\">Errore: username già registrato. Scegline un altro!</p>";
            break;
        default:
            $templateParams["regoutcome"] = "<p class=\"text-danger m-3\">C'è stato un errore nella registrazione. Riprova più tardi!</p>";
            break;
    }
}

require("./template/base.php");
?>