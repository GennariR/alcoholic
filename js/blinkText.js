$(document).ready(function() {
    blink_text();
    setInterval(blink_text, 2000);
});

function blink_text() {
    $('.blink').fadeOut(1000);
    $('.blink').fadeIn(1000);
}