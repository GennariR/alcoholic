$(document).ready(function() {

    var fileInput = document.getElementById('productImage');
    var fileDisplayArea = document.getElementById('imgPreviewArea');

    fileInput.addEventListener('change', function(e) {
        let file = fileInput.files[0];
        let imageType = /image.*/;
        if (file.type.match(imageType)) {
            let reader = new FileReader();
            reader.onload = function(e) {
                fileDisplayArea.innerHTML = "";
                let img = new Image();
                img.src = reader.result;
                img.width = 420;
                fileDisplayArea.appendChild(img);
            }
            reader.readAsDataURL(file); 
        } else {
            fileDisplayArea.innerHTML = "File non supportato!"
        }
    });

});